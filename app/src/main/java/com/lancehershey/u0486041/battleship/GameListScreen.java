package com.lancehershey.u0486041.battleship;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Lance on 10/25/2016.
 */
public class GameListScreen extends Fragment implements ListAdapter, AdapterView.OnItemClickListener,
    Button.OnClickListener {

    private ListView _gameListView = null;

    public static GameListScreen newInstance() {
        return new GameListScreen();
    }

    public GameListScreen() {

    }

    @Override
    public void onResume() {
        super.onResume();

        _gameListView.invalidateViews();
    }

    private void loadFromFile() {
        // Load GSON object from file and pass to data model to unpack.
        String fileContent = "";

        try {
            InputStream inputStream = getActivity().openFileInput(getString(R.string.save_file_name));

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder builder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    builder.append(receiveString);
                }

                inputStream.close();
                fileContent = builder.toString();
                // Give to data model to unpack into the game list.
                GameManager.getInstance().readGSON(fileContent);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        loadFromFile(); // First get the GameManager the file data.
        LinearLayout listLayout = new LinearLayout(getContext());
        listLayout.setOrientation(LinearLayout.VERTICAL);

        Button addGameButton = new Button(getContext());
        LinearLayout.LayoutParams addGameButtonLayoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                0
        );
        addGameButton.setText("Start New Game");
        addGameButton.setOnClickListener(this);
        listLayout.addView(addGameButton, addGameButtonLayoutParams);

        _gameListView = new ListView(getContext());
        LinearLayout.LayoutParams gameListViewLayoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                0,
                1
        );
        _gameListView.setAdapter(this);
        _gameListView.setOnItemClickListener(this);
        listLayout.addView(_gameListView);

        return listLayout;
    }

    @Override
    public int getCount() {
        return GameManager.getInstance().getNumberOfGames();
    }

    @Override
    public boolean isEmpty() {
        return GameManager.getInstance().getNumberOfGames() == 0;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView gameSummaryView = new TextView(getContext());
        GameObject game = GameManager.getInstance().getGame(i);
        String status = "";
        if (game.inProgress()) {
            status = "In Progress";
        }
        else {
            status = "Player " + game.getWinningPlayer() + " won!";
        }
        gameSummaryView.setText("Game " + i + ": Player " + game.getPlayer() + "'s turn.\n" +
                status);
        Log.i("Added List Item", "Added game " + i);
        return gameSummaryView;
    }

    @Override
    public int getItemViewType(int i) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return true;
    }

    @Override
    public boolean isEnabled(int i) {
        return true;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver dataSetObserver) {

    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Log.i("Clicked!", "The user selected game " + i);
        Intent showGameIntent = new Intent();
        showGameIntent.setClass(getActivity(), GameScreenActivity.class);
        showGameIntent.putExtra("game", i);
        startActivity(showGameIntent);
    }

    @Override
    public void onClick(View view) {
        GameManager.getInstance().startGame();
        _gameListView.invalidateViews();
        Log.i("Clicked!", "The user selected new game!");
        Intent showGameIntent = new Intent();
        showGameIntent.setClass(getActivity(), GameScreenActivity.class);
        // GameManager's startGame will update the current game.
        showGameIntent.putExtra("game", GameManager.getInstance().getCurrentGame());
        startActivity(showGameIntent);
    }
}

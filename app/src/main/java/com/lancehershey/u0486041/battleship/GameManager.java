package com.lancehershey.u0486041.battleship;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Lance on 10/24/2016.
 */
public class GameManager extends java.lang.Object {
    static GameManager _Instance = null;

    public static GameManager getInstance() {
        if (_Instance == null) {
            _Instance = new GameManager();
        }
        return _Instance;
    }

    private List<GameObject> _gameObjectList = new ArrayList<GameObject>();
    private int _currentGame;

    public int getCurrentGame() {
        return _currentGame;
    }

    public void setCurrentGame(int currentGame) {
        _currentGame = currentGame;
    }

    public int getNumberOfGames() {
        return _gameObjectList.size();
    }

    private GameManager() {

    }

    public String writeGSON() {
        // Package state information as GSON object.
        Gson gson = new Gson();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < _gameObjectList.size(); i++) {
            builder.append(gson.toJson(_gameObjectList.get(i)));
            builder.append("|"); // Use pipes as delimiters.
        }
        String saveString = builder.toString();
        return saveString;
    }

    public void readGSON(String data) {
        // Pull in GSON data to populate list.
        Gson gson = new Gson();
        // Clear the game list just in case.
        _gameObjectList = new ArrayList<GameObject>();
        StringTokenizer tokenizer = new StringTokenizer(data, "|");
        while (tokenizer.hasMoreTokens()) {
            GameObject game = gson.fromJson(tokenizer.nextToken(), GameObject.class);
            _gameObjectList.add(game);
        }
    }

    public void startGame() {
        // Create a new GameObject and store it in the list.
        _gameObjectList.add(new GameObject());
        _currentGame = _gameObjectList.size() - 1;
    }

    public GameObject getGame(int gameIndex) {
        _currentGame = gameIndex;
        return _gameObjectList.get(gameIndex);
    }

    public void updateGame(int x, int y) {
        _gameObjectList.get(_currentGame).processMove(x, y);
    }

    public void deleteGame(int gameIndex) {
        // TODO: Delete the specified game from the list.
    }
}

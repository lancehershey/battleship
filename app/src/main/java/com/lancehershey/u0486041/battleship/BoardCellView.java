package com.lancehershey.u0486041.battleship;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Lance on 10/27/2016.
 */
public class BoardCellView extends View {
    interface OnCellClickedListener {
        void cellClicked(int x, int y);
    }

    private OnCellClickedListener _onCellClickedListener = null;

    private int _cellState = 0;
    private Point _cellCoordinates = new Point();
    private static final int WATER = 0, SHIP = 1, MISS = 2, HIT = 3;
    private static final int WATER_COLOR = Color.BLUE, SHIP_COLOR = Color.GRAY,
            MISS_COLOR = Color.WHITE, HIT_COLOR = Color.RED, OUTLINE_COLOR = Color.BLACK;

    public void setOnCellClickedListener(OnCellClickedListener listener) {
        _onCellClickedListener = listener;
    }

    public OnCellClickedListener getOnCellClickedListener() {
        return _onCellClickedListener;
    }

    public void setCellState(int state) {
        _cellState = state;
        invalidate();
    }

    public int getCellState() {
        return _cellState;
    }

    public Point getCellCoordinates() {
        return _cellCoordinates;
    }

    public void setCellCoordinates(int x, int y) {
        _cellCoordinates.x = x;
        _cellCoordinates.y = y;
    }

    public BoardCellView(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        RectF cellRect = new RectF();
        cellRect.left = getPaddingLeft();
        cellRect.top = getPaddingTop();
        cellRect.right = getWidth() - getPaddingRight();
        cellRect.bottom = getHeight() - getPaddingBottom();

        Paint cellPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        switch (_cellState) {
            case WATER:
                cellPaint.setColor(WATER_COLOR);
                canvas.drawRect(cellRect, cellPaint);
                break;
            case SHIP:
                cellPaint.setColor(SHIP_COLOR);
                canvas.drawRect(cellRect, cellPaint);
                break;
            case MISS:
                cellPaint.setColor(WATER_COLOR);
                canvas.drawRect(cellRect, cellPaint);
                cellPaint.setColor(MISS_COLOR);
                canvas.drawOval(cellRect, cellPaint);
                break;
            case HIT:
                cellPaint.setColor(SHIP_COLOR);
                canvas.drawRect(cellRect, cellPaint);
                cellPaint.setColor(HIT_COLOR);
                canvas.drawOval(cellRect, cellPaint);
                break;
        }

        Paint borderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        borderPaint.setColor(OUTLINE_COLOR);
        borderPaint.setStyle(Paint.Style.STROKE);
        borderPaint.setStrokeWidth(cellRect.height() * 0.05f);
        canvas.drawRect(cellRect, borderPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean superEvent = super.onTouchEvent(event);

        int x = getCellCoordinates().x;
        int y = getCellCoordinates().y;

        Log.i("Clicked!", "Touch detected at: " + x + ", " + y);

        if (_onCellClickedListener != null) {
            _onCellClickedListener.cellClicked(x, y);
        }

        return superEvent;
    }
}

package com.lancehershey.u0486041.battleship;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;


public class GameListActivity extends AppCompatActivity {
    public static int MASTER_FRAME_ID = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameLayout gameListLayout = new FrameLayout(this);
        setContentView(gameListLayout);

        gameListLayout.setId(MASTER_FRAME_ID);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        GameListScreen gameListScreen = (GameListScreen)getSupportFragmentManager().
                findFragmentByTag(getString(R.string.GAMELIST_FRAGMENT_TAG));
        if (gameListScreen == null) {
            gameListScreen = GameListScreen.newInstance();
            transaction.add(gameListLayout.getId(), gameListScreen, getString(R.string.GAMELIST_FRAGMENT_TAG));
        }
        else {
            transaction.replace(gameListLayout.getId(), gameListScreen);
        }
        transaction.commit();
    }
}

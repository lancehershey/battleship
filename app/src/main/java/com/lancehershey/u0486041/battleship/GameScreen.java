package com.lancehershey.u0486041.battleship;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Lance on 10/25/2016.
 */
public class GameScreen extends Fragment implements BoardCellView.OnCellClickedListener, GameObject.OnGameUpdateListener{
    GameObject _game = null;
    BoardLayout _playerBoard = null, _opponentBoard = null;

    public static GameScreen newInstance() {
        return new GameScreen();
    }

    public GameScreen() {

    }

    private void generateBoard(BoardLayout boardLayout, boolean clickable) {
        for (int i = 0; i < boardLayout.getBoardHeight(); i++) {
            for (int j = 0; j < boardLayout.getBoardWidth(); j++) {
                BoardCellView boardCellView = new BoardCellView(getContext());
                boardCellView.setCellCoordinates(j, i); // x = j, y = i;
                if (clickable) {
                    boardCellView.setOnCellClickedListener(this);
                }
                boardLayout.addView(boardCellView);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        _game = GameManager.getInstance().getGame(GameManager.getInstance().getCurrentGame());
        _game.setOnGameUpdateListener(this);

        LinearLayout screenLayout = new LinearLayout(getContext());
        screenLayout.setOrientation(LinearLayout.VERTICAL);

        _playerBoard = new BoardLayout(getContext());
        LinearLayout.LayoutParams playerBoardLayoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                0,
                1
        );
        generateBoard(_playerBoard, false);
        _playerBoard.setBoard(_game.getBoard(_game.getPlayer()), false);
        screenLayout.addView(_playerBoard, playerBoardLayoutParams);

        _opponentBoard = new BoardLayout(getContext());
        LinearLayout.LayoutParams opponentBoardLayoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                0,
                3
        );
        generateBoard(_opponentBoard, true);
        _opponentBoard.setBoard(_game.getBoard(_game.getOpponent()), true);
        screenLayout.addView(_opponentBoard, opponentBoardLayoutParams);

        return screenLayout;
    }

    @Override
    public void cellClicked(int x, int y) {
        GameManager.getInstance().updateGame(x, y);
        Log.i("Cell Clicked Listener", "GameManager told to update game " +
                GameManager.getInstance().getCurrentGame() + " at position: " + x + ", " + y);
    }

    @Override
    public void gameUpdate(int x, int y, int result) {
        _opponentBoard.updateBoard(x, y, result);
        Log.i("Game Update Listener", "GameManager Responded with a " + result + " at position: " +
                x + ", " + y);
        _playerBoard.setBoard(_game.getBoard(_game.getPlayer()), false);
        _opponentBoard.setBoard(_game.getBoard(_game.getOpponent()), true);
//        saveToFile();
    }

    private void saveToFile() {
        // Get GSON object from data model to write to the file.
        String fileContent = GameManager.getInstance().writeGSON();

        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    getActivity().openFileOutput(getString(R.string.save_file_name), Context.MODE_PRIVATE));
            outputStreamWriter.write(fileContent);
            outputStreamWriter.close();
        } catch (FileNotFoundException e) {
            Log.i("File not found", "Could not open file!");
            e.printStackTrace();
        } catch (IOException e) {
            Log.i("Write Failed", "Write failed!");
            e.printStackTrace();
        }

    }
}

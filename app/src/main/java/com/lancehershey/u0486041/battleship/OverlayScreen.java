package com.lancehershey.u0486041.battleship;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Lance on 11/1/2016.
 */
public class OverlayScreen extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        // This needs to be the fragment view we want to see.
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}

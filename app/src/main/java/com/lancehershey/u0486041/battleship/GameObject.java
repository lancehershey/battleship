package com.lancehershey.u0486041.battleship;

import android.util.Log;

import java.util.Random;

/**
 * Lance Hershey
 * u0486041
 * CS 4530
 * 10/24/2016
 * Battleship GameObject
 * This class handles all of the game logic for a single instance of the game Battleship.
 */
public class GameObject {
    // Turn logs on or off by changing this value.
    private boolean _debug = false;

    interface OnGameUpdateListener {
        void gameUpdate(int x, int y, int result);
    }

    private OnGameUpdateListener _onGameUpdateListener = null;

    public OnGameUpdateListener getOnGameUpdateListener() {
        return _onGameUpdateListener;
    }

    public void setOnGameUpdateListener(OnGameUpdateListener listener) {
        _onGameUpdateListener = listener;
    }

    // 0 = open water, 1 = ship, 2 = miss, 3 = hit
    private static final int WATER = 0, SHIP = 1, MISS = 2, HIT = 3;
    private int[][][] _gameBoards = new int[2][10][10]; // 2 10x10 grids
    private boolean _validBoard;
    private boolean _player1turn;
    private boolean _inProgress;
    private int _winningPlayer = 0;

    public GameObject() {
        gameBoardSetup(_gameBoards[0]); // Place ships for player 1.
        gameBoardSetup(_gameBoards[1]); // Place ships for player 2.

        if (_debug) {
            Log.i("Generated Boards", "Board generation for both players complete!");
        }

        _inProgress = true;
        _player1turn = true; // Always start with player 1.
    }

    public boolean inProgress() {
        return _inProgress;
    }

    public int[][] getBoard(int player) {
        // player will be a 1 or 2, so -1 to get board index.
        return _gameBoards[player - 1];
    }

    public int getPlayer() {
        if (_player1turn) {
            return 1;
        }
        else {
            return 2;
        }
    }

    public int getOpponent() {
        if (_player1turn) {
            return 2;
        }
        else {
            return 1;
        }
    }

    public int getWinningPlayer() {
        return _winningPlayer;
    }

    public void gameBoardSetup(int[][] board) {
        // Place ships in random locations until a valid configuration is found.
        _validBoard = false;
        while (!_validBoard) {
            resetGameBoard(board);
            placeShipsRandomly(board);
        }
        if (_debug) {
            Log.i("Board Generated", "Generated new board!");
            logBoard(board);
        }
    }

    public void resetGameBoard(int[][] board) {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                board[i][j] = WATER;
            }
        }
        if (_debug) {
            Log.i("Board Reset", "Reset board!");
            logBoard(board);
        }
    }

    private void placeShipsRandomly(int[][] board) {
        // Place Carrier (5 squares)
        placeShip(board, 5); // This placement should always be valid if placeShip is correct.
        // Place Battleship (4 squares)
        placeShip(board, 4);
        if (!_validBoard) return;
        // Place Cruiser (3 squares)
        placeShip(board, 3);
        if (!_validBoard) return;
        // Place Submarine (3 squares)
        placeShip(board, 3);
        if (!_validBoard) return;
        // Place Destroyer (2 squares)
        placeShip(board, 2); // If this placement invalidates the board, it will return anyway.
    }

    private void placeShip(int[][] board, int size) {
        Random rand = new Random();
        boolean horizontal = rand.nextBoolean();
        int x, y;
        // Generate random locations such that the ships will not run off the edges of the board.
        if (horizontal) {
            x = rand.nextInt(board.length - size);
            y = rand.nextInt(board[x].length);
            for (int j = x; j < x + size; j++) {
                // If at any point the location we are writing to is already 1, reset and try again.
                if (board[y][j] == SHIP) {
                    if (_debug) {
                        Log.i("Invalid Location", "Invalid Location! " + "x: " + x + " y: " + y +
                                " size: " + size + " orientation: horizontal");
                    }
                    _validBoard = false;
                    return;
                }
                board[y][j] = SHIP;
            }
        }
        else {
            x = rand.nextInt(board.length);
            y = rand.nextInt(board[x].length - size);
            for (int i = y; i < y + size; i++) {
                // If at any point the location we are writing to is already 1, reset and try again.
                if (board[i][x] == SHIP) {
                    if (_debug) {
                        Log.i("Invalid Location", "Invalid Location! " + "x: " + x + " y: " + y +
                                " size: " + size + " orientation: vertical");
                    }
                    _validBoard = false;
                    return;
                }
                board[i][x] = SHIP;
            }
        }
        // This point is hit only if the current ship placement is valid.
        _validBoard = true;
    }

    public void processMove(int x, int y) {
        // Check move against specified player's board and return hit(3) or miss(2).
        int player = getPlayer() - 1; // getCurrentPlayer returns 1 or 2, so -1 to get index.
        int result;
        if (_gameBoards[player][y][x] == SHIP) {
            result = HIT; // Hit = 3
        }
        else {
            result = MISS; // Miss = 2
        }
        // Update game board.
        _gameBoards[player][y][x] = result;
        // Send message to game update listener based on result.
        if (_onGameUpdateListener != null) {
            _onGameUpdateListener.gameUpdate(x, y, result);
        }
        // Next player's turn.
        _player1turn = !_player1turn;
    }

    public boolean gameOver() {
        // Check to see if a player's ships have all been sunk. If so, the other player wins.
        int[][] player1board = _gameBoards[0];
        int[][] player2board = _gameBoards[1];

        if (playerDefeated(player1board)) {
            _inProgress = false;
            _winningPlayer = 2;
        }
        else if (playerDefeated(player2board)) {
            _inProgress = false;
            _winningPlayer = 1;
        }
        return !_inProgress;
    }

    private boolean playerDefeated(int[][] board) {
        // Just walk over the board and check for 1's. If there are none, all ships have been sunk.
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j] == SHIP) {
                    return false;
                }
            }
        }
        return true;
    }

    // Print the board state for debugging purposes.
    private void logBoard(int[][] board) {
        Log.i("Board Output Begin", "Board Values: ");
        for (int i = 0; i < board.length; i++) {
            StringBuilder builder = new StringBuilder();
            builder.append("Row " + i + ":");
            for (int j = 0; j < board[i].length; j++) {
                builder.append(" " + board[i][j]);
            }
            String rowString = builder.toString();
            Log.i("Board Output", rowString);
        }
        Log.i("Board Output End", "Board Output Complete");
    }

}

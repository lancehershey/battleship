package com.lancehershey.u0486041.battleship;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;

/**
 * Created by Lance on 10/27/2016.
 */
public class GameScreenActivity extends AppCompatActivity{
    public static int DETAIL_FRAME_ID = 101;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().hasExtra("game")) {
            int gameIndex = getIntent().getExtras().getInt("game");
            Log.i("Game Open Open", "Opened game " + gameIndex);
            GameManager.getInstance().setCurrentGame(gameIndex);
        }
        else {
            GameManager.getInstance().startGame();
            Log.i("Game Create", "Created new game!");
        }

        FrameLayout gameLayout = new FrameLayout(this);
        setContentView(gameLayout);

        gameLayout.setId(DETAIL_FRAME_ID);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        GameScreen gameScreen = (GameScreen)getSupportFragmentManager().
                findFragmentByTag(getString(R.string.GAMESCREEN_FRAGMENT_TAG));
        if (gameScreen == null) {
            gameScreen = GameScreen.newInstance();
            transaction.add(gameLayout.getId(), gameScreen, getString(R.string.GAMESCREEN_FRAGMENT_TAG));
        }
        else {
            transaction.replace(gameLayout.getId(), gameScreen);
        }
        transaction.commit();
    }
}
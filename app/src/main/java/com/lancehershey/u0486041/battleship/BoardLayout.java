package com.lancehershey.u0486041.battleship;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Lance on 10/25/2016.
 */
public class BoardLayout extends ViewGroup {
//    private int[][] _board = new int[10][10]; // Game is played on a 10x10 grid.
    private BoardCellView[][] _board = new BoardCellView[10][10];
    private static final int WATER = 0, SHIP = 1, MISS = 2, HIT = 3;

    public BoardLayout(Context context) {
        super(context);
    }

    public int getBoardHeight() {
        return _board.length;
    }

    public int getBoardWidth() {
        return _board.length;
    }

    @Override
    public void addView(View child) {
        if (getChildCount() < _board.length * _board.length) {
            super.addView(child);
            if (child instanceof BoardCellView) {
                BoardCellView cellView = (BoardCellView)child;
                int x = cellView.getCellCoordinates().x;
                int y = cellView.getCellCoordinates().y;
                _board[y][x] = cellView;
            }
        }
    }

    @Override
    protected void onLayout(boolean b, int i, int i1, int i2, int i3) {
        for (int childIndex = 0; childIndex < getChildCount(); childIndex++) {

            // Get density of the display
//            float density = getResources().getDisplayMetrics().density;
//            float childWidth = 0.3f * 160.0f * density; // DPs are 1/160th of an inch.
//            float childHeight = 0.2f * 160.0f * density;

            float width = getWidth();
            float height = getHeight();

            if (height < width) {
                width = height;
            }
            else {
                height = width;
            }

            float childWidth = width / _board.length;
            float childHeight = height / _board.length;
            int x = 0, y = 0;

            PointF childCenter = new PointF();
            if (getChildAt(childIndex) instanceof BoardCellView) {
                BoardCellView cellView = (BoardCellView)getChildAt(childIndex);
                x = cellView.getCellCoordinates().x;
                y = cellView.getCellCoordinates().y;
            }

            childCenter.x = (float)x * childWidth + (childWidth * 0.5f);
            childCenter.y = (float)y * childHeight + (childHeight * 0.5f);

            Rect childRect = new Rect();
            childRect.left = (int) (childCenter.x - childWidth * 0.5f);
            childRect.right = (int) (childCenter.x + childWidth * 0.5f);
            childRect.top = (int) (childCenter.y - childHeight * 0.5f);
            childRect.bottom = (int) (childCenter.y + childHeight * 0.5f);

            View childView = getChildAt(childIndex);
            childView.layout(childRect.left, childRect.top, childRect.right, childRect.bottom);
        }
    }

    public void setBoard(int[][] board, boolean hideShips) {
        if (!hideShips) {
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[i].length; j++) {
                    updateBoard(j, i, board[i][j]);
                }
            }
        }
        else {
            for (int i = 0; i < board.length; i++) {
                for (int j = 0; j < board[i].length; j++) {
                    if (board[i][j] == SHIP) {
                        updateBoard(j, i, WATER); // Hide ships for opposing player.
                    }
                    else {
                        updateBoard(j, i, board[i][j]);
                    }
                }
            }
        }
    }

    public void updateBoard(int x, int y, int value) {
        _board[y][x].setCellState(value);
    }

}
